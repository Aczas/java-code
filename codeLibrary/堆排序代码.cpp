#include <iostream>
#include <algorithm>
using namespace std;
void max_heapify(int arr[], int dad, int end)//将数组初始化为大根堆
{
    //建立父节点下标和子节点下标
    int son = dad * 2 + 1;
    while (son <= end)  //若子节点下标在范围内则进行比较
    {
        if (son + 1 <= end && arr[son] < arr[son + 1]) //先比较两个子节点大小，选择较大的
            son++;
        if (arr[dad] > arr[son]) //如果父节点大于子节点表示调整完毕
            break;
        else  //否则交换父子内容再继续子节点和孙节点的比较以保证每次取出最大数之后堆仍是大根堆
        {
            swap(arr[dad], arr[son]);
            dad = son;
            son = dad * 2 + 1;
        }
    }
}
void heap_sort(int arr[], int len)//堆排序
{
    //初始化堆，从最后一个父节点开始调整
    for (int i = len / 2 - 1; i >= 0; i--)
        max_heapify(arr, i, len - 1);
    //将根节点元素和最后一个叶子节点元素做交换，再调整堆为大根堆，直到排序完毕
    for (int i = len - 1; i > 0; i--)
    {
        swap(arr[0], arr[i]);
        max_heapify(arr, 0, i - 1);
    }
}
void main()
{
    int arr[] = { 29, 5, 25, 17, 28, 24, 11, 39, 8, 6, 13, 35, 9, 38, 27, 18, 1, 8, 19, 7, 3, 12, 26, 5, 9, 7, 4, 36, 2 };
    int len = (int)sizeof(arr) / sizeof(*arr);
    /*
    arr表示int类型数组占用的空间大小
    *arr表示arr数组中第一个元素所占的空间大小
    两者相除并转换类型即为arr数组的长度
    */
    heap_sort(arr, len);//利用堆排序对arr数组进行排序
    for (int i = 0; i < len; i++)
        cout << arr[i] << ' ';
    cout << endl;
}