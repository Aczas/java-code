package commodities;

public class shoes extends commodity{//鞋类（继承于商品类）
	private  String color;//颜色
	private  String size;//尺寸
	
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public shoes(Integer id,String name,String category,Integer price,String color,String size) {		
		super(id,name,category,price);
		this.color=color;
		this.size=size;
	}
	@Override
	public String toString() {
		return shoes.super.getName()+" "+"鞋类"+" "+"单价："+shoes.super.getPrice()+"元 "+shoes.this.color+" "+shoes.this.size;
	}
	
}
