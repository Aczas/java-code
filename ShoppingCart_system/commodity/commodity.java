package commodities;

public class commodity {//商品类（父类）
	private Integer id;//商品编号
	private String name;//商品名称
	private String category;//商品种类
	private Integer price;//商品价钱
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "commodity [id=" + id + ", name=" + name + ", category=" + category + ", price=" + price + "]";
	}

	public commodity(Integer id,String name,String category,Integer price) {
		this.id =id;
		this.name =name;
		this.category=category;
		this.price=price;
	}
	
}
