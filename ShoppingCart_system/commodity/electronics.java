package commodities;

public class electronics extends commodity{//电子产品类（继承于商品类）
	
	private String color;//颜色
	private String rom;//内存
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getRom() {
		return rom;
	}
	public void setRom(String rom) {
		this.rom = rom;
	}
	
	public electronics(Integer id,String name,String category,Integer price,String color,String rom) {		
		super(id,name,category,price);
		this.color=color;
		this.rom=rom;
	}
	@Override
	public String toString() {
		return electronics.super.getName()+" "+"电子产品类"+" "+"单价："+electronics.super.getPrice()+"元 "+electronics.this.color+" "+electronics.this.rom;
	}
	
}
