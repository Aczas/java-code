package commodities;

public class commodity_message {//商品信息类
	private commodity good;//要购买的商品
	private int count;//要购买的数量
	
	public commodity_message(commodity good,int count) {
		this.good=good;
		this.count=count;
	}

	public commodity getGood() {
		return good;
	}

	public void setGood(commodity good) {
		this.good = good;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	public int totalMoney() {
		int  price = good.getPrice();
		return price * count; 
	}

	@Override
	public String toString() {
		return this.good.toString()+" 数量: "+this.count+" 小计: "+this.totalMoney()+"元";
	}
}

