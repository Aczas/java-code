package commodities;

public class books extends commodity {//书籍类（继承于商品类）
	private String author;//作者
	private String press;//出版社
	
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPress() {
		return press;
	}
	public void setPress(String press) {
		this.press = press;
	}
	
	public books(Integer id,String name,String category,Integer price,String author,String press) {		
		super(id,name,category,price);
		this.author=author;
		this.press=press;
	}
	@Override
	public String toString() {
		return books.super.getName()+" "+"书籍类"+" "+"单价："+books.super.getPrice()+"元 "+books.this.author+" "+books.this.press;
	}
}
