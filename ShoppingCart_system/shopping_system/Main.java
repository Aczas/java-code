package shopping_System;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {

		Scanner cin = new Scanner(System.in);
		int choice;//输入的选项
		int id;//输入的商品编号
		int num;//输入的商品数量

		while (true) {
			Menu.show_main_menu();
			choice = cin.nextInt();
			switch (choice) {
			case 1:// 进入商城进行操作
				while (true) {
					if (choice == 4)
						break;
					Menu.show_shelf();;
					choice = cin.nextInt();
					switch (choice) {
					case 1:// 商城菜单
						Shelf_lmpl.showshelf();;
						break;
					case 2://查找商品
						System.out.println("请输入要查找商品的id: ");
						id=cin.nextInt();
						if (Shelf_lmpl.search_shelf(id))
							;
						else
							System.out.println("不存在该商品!");
						break;
					case 3:// 购买商品
						System.out.println("请输入商品ID和数量:");
						id = cin.nextInt();
						num = cin.nextInt();
						Shelf_lmpl.shop(id, num);;
						System.out.println("商品添加成功!");
						break;
					case 4:
						break;
					default:
						System.out.println("请重新输入: ");
					}
				}
				break;
			case 2:// 进入购物车进行操作
				while (true) {
					if(choice==6)
						break;
					Menu.show_cart();
					choice = cin.nextInt();
					switch (choice) {
					case 1:// 购物车一览
						Cart_lmpl.showcart();
						break;
					case 2://查找商品
						System.out.println("请输入要查找商品的id: ");
						id=cin.nextInt();
						if (Cart_lmpl.search_cart(id))
							;
						else
							System.out.println("不存在该商品!");
						break;
					case 3://删除商品
						System.out.println("请输入要删除的商品Id和数量: ");
						id=cin.nextInt();
						num=cin.nextInt();
						Cart_lmpl.reduceGood(id, num);
						System.out.println("删除成功!");
						break;
					case 4://清空购物车
						Cart_lmpl.clearCart();;
						 System.out.println("清空购物车成功!");
						break;
					case 5://结算
						System.out.println("本次共消费: "+Cart_lmpl.totalAllMoney()+"元");
						break;
					case 6:// 返回主菜单
					default:
						System.out.println("请重新输入: ");
					}
				}
				break;
			case 3:
				System.out.println("退出成功");
				cin.close();
				System.exit(0);
			default:
				System.out.println("请重新输入: ");
			}
		}
	}
}