package shopping_System;

public class Menu {
	/*展示购物车系统菜单*/
	public static void show_main_menu() {
		System.out.println("***        【1】进入商城");
		System.out.println("***        【2】进入购物车");
		System.out.println("***        【3】退出");
		System.out.println("---              请选择： ");
	}
	/*展示商城菜单*/
		public static void show_shelf() {
			System.out.println("***        【1】商品一览 ");
			System.out.println("***        【2】查找商品 ");
			System.out.println("***        【3】购买商品 ");
			System.out.println("***        【4】回到主菜单");
			System.out.println("---              请选择： ");
		}
	/*展示购物车菜单*/
	public static void show_cart() {
		System.out.println("***        【1】购物车商品一览");
		System.out.println("***        【2】查找商品 ");
		System.out.println("***        【3】删除商品 ");
		System.out.println("***        【4】清空购物车 ");
		System.out.println("***        【5】结算");
		System.out.println("***        【6】返回主菜单");
		System.out.println("---              请选择：                 ");
	}
}
