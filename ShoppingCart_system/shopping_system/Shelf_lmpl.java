package shopping_System;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import commodities.books;
import commodities.commodity;
import commodities.commodity_message;
import commodities.electronics;
import commodities.shoes;

public class Shelf_lmpl implements DAO.shelfDao{

	public static Map<Integer, commodity> mapshelf = new LinkedHashMap<Integer, commodity>();
	public static Map<Integer, commodity_message> mapcart = new LinkedHashMap<Integer, commodity_message>();

		static {
		electronics good1 = new electronics(1, "小米10", "电子产品", 3999, "白色", "8G+128G");
		electronics good2 = new electronics(2, "联想y7000p", "电子产品", 8499, "黑色", "16G+512G");
		electronics good3 = new electronics(3, "iPadPro", "电子产品", 6229, "灰色", "128G");
		electronics good4 = new electronics(4, "switch", "电子产品", 2996, "红蓝色", "32G");
		electronics good5 = new electronics(5, "kindle第4代", "电子产品", 998, "墨黑色", "8G");
		shoes good6 = new shoes(6, "Nike Kyrie 7", "鞋类", 624, "黑色", "38");
		shoes good7 = new shoes(7, "Air Jordan 1", "鞋类", 965, "红色", "39");
		shoes good8 = new shoes(8, "李宁韦德之道", "鞋类", 468, "黑色", "40");
		shoes good9 = new shoes(9, "匹克闪现1代", "鞋类", 279, "黑白", "41");
		shoes good10 = new shoes(10, "安踏KT6", "鞋类", 456, "水墨", "42");
		books good11 = new books(11, "《嫌疑人X的献身》", "书籍类", 24, "东野圭吾", "南海出版社");
		books good12 = new books(12, "《三体》1-3", "书籍类", 83, "刘慈欣", "重庆出版社");
		books good13 = new books(13, "《Java学习笔记》", "书籍类", 98, "林信良", "清华大学出版社");
		books good14 = new books(14, "《围城》", "书籍类", 28, "钱钟书", "人民文学出版社");
		books good15 = new books(15, "《活着》", "书籍类", 19, "余华", "作家出版社");
		mapshelf.put(good1.getId(), good1);
		mapshelf.put(good2.getId(), good2);
		mapshelf.put(good3.getId(), good3);
		mapshelf.put(good4.getId(), good4);
		mapshelf.put(good5.getId(), good5);
		mapshelf.put(good6.getId(), good6);
		mapshelf.put(good7.getId(), good7);
		mapshelf.put(good8.getId(), good8);
		mapshelf.put(good9.getId(), good9);
		mapshelf.put(good10.getId(), good10);
		mapshelf.put(good11.getId(), good11);
		mapshelf.put(good12.getId(), good12);
		mapshelf.put(good13.getId(), good13);
		mapshelf.put(good14.getId(), good14);
		mapshelf.put(good15.getId(), good15);
	}

	// 商品一览
	public static void showshelf() {
		Set keySet = mapshelf.keySet();
		Iterator it = keySet.iterator();
		while (it.hasNext()) {
			Object key = it.next();
			Object value = mapshelf.get(key);
			System.out.println(key + ":" + value);
		}
	}

	public static boolean search_shelf(int id) {
		if (mapshelf.containsKey(id)) {
			System.out.println(mapshelf.get(id));
			return true;
		} else
			return false;
	}
	//添加购物车
	public static void shop(int id, int num) {
		if (mapcart.containsKey(id)) {
			commodity_message goodItem = mapcart.get(id);
			goodItem.setCount(goodItem.getCount() + num);
		} else
			mapcart.put(id, new commodity_message(mapshelf.get(id), num));
	}
}
