package shopping_System;

import commodities.commodity_message;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class Cart_lmpl implements DAO.cartDao{

	public static void showcart() {//展示购物车
		if (Shelf_lmpl.mapcart.isEmpty()) {//判断购物车是否为空
			System.out.println("购物车为空！");
		}
		else {
			Set keySet=Shelf_lmpl.mapcart.keySet();
			Iterator it=keySet.iterator();
			while(it.hasNext())	{//打印购物车
				Object key=it.next();
				Object value=Shelf_lmpl.mapcart.get(key);
				System.out.println("商品编号:"+key+" "+value);
		}
		}
	}
	public static boolean search_cart(int id) {
		if (Shelf_lmpl.mapcart.containsKey(id)) {
			System.out.println(Shelf_lmpl.mapcart.get(id).getGood());
			return true;
		} else
			return false;
	}
	public static boolean reduceGood(int goodId,int count) {//删除商品
		commodity_message goodItem = Shelf_lmpl.mapcart.get(goodId);
		if (Shelf_lmpl.mapcart.containsKey(goodId)) {
			if(count>goodItem.getCount()) {//输入数量>已有数量
				System.out.println("您的购物车里只有"+goodItem.getCount()+"件该商品，请不要输入超过该数量。");
				return false;
			}else if (count == goodItem.getCount()) {//输入数量=已有数量
				Shelf_lmpl.mapcart.remove(goodId);//直接将该商品移除map
				return true;
			} else if (count < goodItem.getCount()) {//输入数量<已有数量
					goodItem.setCount(goodItem.getCount()-count);
					return true;
			}
		}
		else System.out.println("购物车中没有该货物");
		return false;
	}
	public static void clearCart() {// 清空购物车
		Shelf_lmpl.mapcart.clear();
	}
	public static int totalAllMoney() {// 结算
		int totalmoney = 0;
		Collection<commodity_message> GoodItems = Shelf_lmpl.mapcart.values();
		Iterator<commodity_message> iterator = GoodItems.iterator();
		while (iterator.hasNext()) {
			commodity_message GoodItem = iterator.next();
			int money = GoodItem.totalMoney();
			totalmoney += money;
		}
		return totalmoney;
	}
}
