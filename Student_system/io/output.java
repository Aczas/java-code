package io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class output {
	static FileWriter fw=null;
	static PrintWriter fw1=null;
	
	public static boolean out_to(String file,String str,boolean k) {//每次写入一个传入的字符串
		try {
			
			fw=new FileWriter(file,k);
			fw1=new PrintWriter(fw);
			fw1.write(str);
			return true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(fw!=null)
				try {
					fw.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if(fw1!=null)
				fw1.close();
		}
		return false;
	}

}
