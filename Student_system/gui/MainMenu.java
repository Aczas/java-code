package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import student.Date;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import java.awt.Font;

public class MainMenu extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainMenu frame = new MainMenu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainMenu() {
		setTitle("\u5B66\u751F\u57FA\u672C\u4FE1\u606F\u7BA1\u7406\u7CFB\u7EDF");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 535, 391);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnNewButton = new JButton("\u6DFB\u52A0\u5B66\u751F\u4FE1\u606F");
		btnNewButton.setFont(new Font("����", Font.PLAIN, 18));
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new AddOperation().setVisible(true);
				dispose();
			}
		});
		btnNewButton.setBounds(26, 71, 200, 45);
		contentPane.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("\u5220\u9664\u5B66\u751F\u4FE1\u606F");
		btnNewButton_1.setFont(new Font("����", Font.PLAIN, 18));
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new DeleteOperation().setVisible(true);
				dispose();
			}
		});
		btnNewButton_1.setBounds(290, 164, 200, 45);
		contentPane.add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("\u67E5\u627E\u5B66\u751F\u4FE1\u606F");
		btnNewButton_2.setFont(new Font("����", Font.PLAIN, 18));
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {// ����
				new SearchOperation().setVisible(true);
				dispose();
			}
		});
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_2.setBounds(290, 71, 200, 45);
		contentPane.add(btnNewButton_2);

		JButton btnNewButton_3 = new JButton("\u663E\u793A\u6240\u6709\u4FE1\u606F");
		btnNewButton_3.setFont(new Font("����", Font.PLAIN, 18));
		btnNewButton_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {// ���
				new DisplayOperation().setVisible(true);
				dispose();
				Date.display();
			}
		});
		btnNewButton_3.setBounds(26, 261, 200, 45);
		contentPane.add(btnNewButton_3);

		JButton btnNewButton_4 = new JButton("\u4FDD\u5B58\u5E76\u9000\u51FA");
		btnNewButton_4.setFont(new Font("����", Font.PLAIN, 18));
		btnNewButton_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Date.flush();
				dispose();
			}
		});
		btnNewButton_4.setBounds(290, 261, 200, 45);
		contentPane.add(btnNewButton_4);

		JButton btnNewButton_5 = new JButton("\u4FEE\u6539\u5B66\u751F\u4FE1\u606F");
		btnNewButton_5.setFont(new Font("����", Font.PLAIN, 18));
		btnNewButton_5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new ModifyOperation().setVisible(true);
				dispose();

			}

		});
		btnNewButton_5.setBounds(26, 164, 200, 45);
		contentPane.add(btnNewButton_5);

		JLabel lblNewLabel = new JLabel("\u5B66\u751F\u4FE1\u606F\u7BA1\u7406\u7CFB\u7EDF");
		lblNewLabel.setFont(new Font("��Բ", Font.PLAIN, 24));
		lblNewLabel.setBounds(160, 13, 232, 27);
		contentPane.add(lblNewLabel);
	}
}
