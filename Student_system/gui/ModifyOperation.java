package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import student.Date;

import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class ModifyOperation extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ModifyOperation frame = new ModifyOperation();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ModifyOperation() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 681, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnNewButton = new JButton("\u8FD4\u56DE");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new MainMenu().setVisible(true);
				dispose();
			}
		});
		btnNewButton.setBounds(305, 213, 113, 27);
		contentPane.add(btnNewButton);

		JLabel lblNewLabel = new JLabel("\u8BF7\u8F93\u5165\u8981\u4FEE\u6539\u7684\u5B66\u751F\u5B66\u53F7:");
		lblNewLabel.setBounds(87, 35, 162, 15);
		contentPane.add(lblNewLabel);

		textField = new JTextField();
		textField.setBounds(87, 60, 174, 21);
		contentPane.add(textField);
		textField.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("\u8BF7\u8F93\u5165\u4FEE\u6539\u540E\u7684\u5B66\u751F\u4FE1\u606F");
		lblNewLabel_1.setBounds(87, 102, 162, 15);
		contentPane.add(lblNewLabel_1);

		textField_1 = new JTextField();
		textField_1.setBounds(87, 127, 174, 21);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel(
				"\u8BF7\u6309\\\"\u5B66\u53F7,\u59D3\u540D,\u6027\u522B,\u51FA\u751F\u65E5\u671F,\u653F\u6CBB\u9762\u8C8C,\u5BB6\u5EAD\u4F4F\u5740,\u7535\u8BDD,\u5BBF\u820D\u53F7\\\"\u7684\u683C\u5F0F\u8F93\u5165\u5B66\u751F\u4FE1\u606F:");
		lblNewLabel_2.setBounds(87, 174, 518, 15);
		contentPane.add(lblNewLabel_2);

		JButton btnNewButton_1 = new JButton("\u786E\u5B9A");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				GUI.Date.str = textField.getText();
				if ((GUI.Date.tempStr = Date.seek(Date.file_students, GUI.Date.str)) == null)
					System.out.println("要修改的学生信息不存在!");
				else {
					if (Date.modify(GUI.Date.tempStr, textField_1.getText()))
						System.out.println("修改成功!");
					else
						System.out.println("修改失败!");
				}
			}
		});
		btnNewButton_1.setBounds(87, 215, 93, 23);
		contentPane.add(btnNewButton_1);
	}
}
