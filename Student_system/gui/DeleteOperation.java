package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import student.Date;

import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class DeleteOperation extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeleteOperation frame = new DeleteOperation();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DeleteOperation() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnNewButton = new JButton("\u8FD4\u56DE");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new MainMenu().setVisible(true);
				dispose();
			}
		});
		btnNewButton.setBounds(305, 213, 113, 27);
		contentPane.add(btnNewButton);

		JLabel lblNewLabel = new JLabel("\u8BF7\u8F93\u5165\u8981\u5220\u9664\u7684\u5B66\u751F\u5B66\u53F7:");
		lblNewLabel.setBounds(88, 56, 247, 15);
		contentPane.add(lblNewLabel);

		textField = new JTextField();
		textField.setBounds(88, 91, 247, 21);
		contentPane.add(textField);
		textField.setColumns(10);

		JButton btnNewButton_1 = new JButton("\u786E\u5B9A");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				GUI.Date.str = textField.getText();
				if ((GUI.Date.tempStr = Date.seek(Date.file_students, GUI.Date.str)) == null)
					System.out.println("要删除的文件不存在!");
				else {
					if (Date.delete(GUI.Date.str))
						System.out.println("删除成功!");
					else
						System.out.println("删除失败!");
				}
			}
		});
		btnNewButton_1.setBounds(56, 215, 93, 23);
		contentPane.add(btnNewButton_1);
	}

}
