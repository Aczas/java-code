package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import student.Date;

import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.AbstractListModel;
import java.awt.Font;

public class DisplayOperation extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DisplayOperation frame = new DisplayOperation();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DisplayOperation() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 686, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 10, 651, 253);
		contentPane.add(panel);
		panel.setLayout(null);

		JList list = new JList((String[]) Date.str_students.toArray(new String[Date.str_students.size()]));
		list.setFont(new Font("����", Font.PLAIN, 15));
		list.setBounds(12, 53, 297, 134);
		list.setVisibleRowCount(7);
		panel.add(list);

		JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.setBounds(64, 33, 523, 136);
		panel.add(scrollPane);

		JButton btnNewButton = new JButton("\u8FD4\u56DE");
		btnNewButton.setBounds(279, 220, 93, 23);
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new MainMenu().setVisible(true);
				dispose();
			}
		});
		panel.add(btnNewButton);
	}

}
