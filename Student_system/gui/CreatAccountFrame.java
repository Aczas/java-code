package GUI;



import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import io.output;
import student.Date;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.concurrent.TimeUnit;

import javax.swing.JPasswordField;
import java.awt.Font;

public class CreatAccountFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreatAccountFrame frame = new CreatAccountFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CreatAccountFrame() {
		setTitle("\u6CE8\u518C");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 417, 346);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u8D26\u53F7\uFF1A");
		lblNewLabel.setFont(new Font("黑体", Font.PLAIN, 23));
		lblNewLabel.setBounds(14, 62, 72, 27);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u5BC6\u7801\uFF1A");
		lblNewLabel_1.setFont(new Font("黑体", Font.PLAIN, 23));
		lblNewLabel_1.setBounds(14, 127, 72, 27);
		contentPane.add(lblNewLabel_1);
		
		textField = new JTextField();
		textField.setBounds(100, 61, 268, 36);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("\u521B\u5EFA\u4E00\u4E2A\u65B0\u8D26\u53F7");
		lblNewLabel_2.setFont(new Font("楷体", Font.PLAIN, 20));
		lblNewLabel_2.setBounds(128, 13, 177, 18);
		contentPane.add(lblNewLabel_2);
		
		JButton btnNewButton = new JButton("\u786E\u5B9A");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				if (String.valueOf(passwordField_1.getPassword()).equals(String.valueOf(passwordField.getPassword()))){
					output.out_to(Date.file_user, textField.getText() + " " + String.valueOf(passwordField.getPassword()) + "\r\n", true);
					System.out.println("注册成功!已为您自动登录...");
					
					new MainMenu().setVisible(true);
					dispose();//进入主界面
					
					
					try {
						TimeUnit.SECONDS.sleep(2);
						;
					} catch (InterruptedException ie) {
					}
				} else {
					System.out.println("密码不一致,请重新输入:");
				}
			}
		});
		btnNewButton.setBounds(53, 258, 113, 27);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("\u8FD4\u56DE");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new Mydemo().setVisible(true);
		        dispose();//回到初始界面
			}
		});
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_1.setBounds(228, 258, 113, 27);
		contentPane.add(btnNewButton_1);
		
		JLabel lblNewLabel_3 = new JLabel("\u786E\u8BA4\uFF1A");
		lblNewLabel_3.setFont(new Font("黑体", Font.PLAIN, 23));
		lblNewLabel_3.setBounds(14, 187, 113, 27);
		contentPane.add(lblNewLabel_3);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(100, 186, 268, 36);
		contentPane.add(passwordField);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(100, 126, 268, 36);
		contentPane.add(passwordField_1);
	}

}
