package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import student.Date;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.concurrent.TimeUnit;
import java.awt.Font;

public class LoginFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginFrame frame = new LoginFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginFrame() {
		setTitle("\u767B\u5F55");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("\u8D26\u53F7\uFF1A");
		lblNewLabel.setFont(new Font("����", Font.PLAIN, 23));
		lblNewLabel.setBounds(68, 41, 72, 27);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("\u5BC6\u7801:");
		lblNewLabel_1.setFont(new Font("����", Font.PLAIN, 23));
		lblNewLabel_1.setBounds(68, 102, 72, 27);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setBounds(141, 203, 198, 36);
		contentPane.add(lblNewLabel_2);

		textField = new JTextField();
		textField.setBounds(141, 41, 217, 36);
		contentPane.add(textField);
		textField.setColumns(10);

		JButton btnNewButton = new JButton("\u767B\u5F55");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (Date.seek(Date.file_user,
						textField.getText() + " " + new String(passwordField.getPassword())) != null) {
					lblNewLabel_2.setText(GUI.Date.log_success);
					new MainMenu().setVisible(true);
					dispose();
				} else {
					lblNewLabel_2.setText(GUI.Date.log_fail);
				}

			}
		});
		btnNewButton.setBounds(60, 163, 113, 27);
		contentPane.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("\u8FD4\u56DE");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new Mydemo().setVisible(true);
				dispose();
			}
		});
		btnNewButton_1.setBounds(245, 163, 113, 27);
		contentPane.add(btnNewButton_1);

		passwordField = new JPasswordField();
		passwordField.setBounds(141, 102, 217, 36);
		contentPane.add(passwordField);

	}
}
