package student;

import java.awt.Component;
import java.util.ArrayList;

import io.input;
import io.output;

public class Date {
	public static String file_user = "C:\\Users\\newid\\Desktop\\user.txt";
	public static String file_students = "C:\\Users\\newid\\Desktop\\students.txt";

	public static ArrayList<String> str_user = new ArrayList<String>();
	public static ArrayList<String> str_students = new ArrayList<String>();

	public static void menu() {
		System.out.println("1-学生信息一览\r\n" + "2-增加学生信息\r\n" + "3-删除学生信息\r\n" + "4-查找用户信息\r\n" + "5-修改学生信息\r\n"
				+ "6-保存并退出\r\n" + "\r\n" + "请输入:");
	}

	/* 装载数据 */
	public static void info_loading() {
		input.from(file_user);
		input.from(file_students);
	}

	public static void flush() {
		int flag = 1;
		for (String e : str_students)
			if (flag == 1) {
				output.out_to(file_students, e + "\r\n", false);
				flag = 0;
			} else
				output.out_to(file_students, e + "\r\n", true);
	}

	public static void display() {
		for (String e : str_students)
			System.out.println(e);
	}

	public static boolean add(String str) {
		return Date.str_students.add(str);
	}

	public static boolean delete(String str) {
		return str_students.remove(str);
	}

	/* 在指定文档里查找指定数据 */
	public static String seek(String file, String str) {
		if (file.equals(file_user))
			for (String e : str_user) {
				if (e.equals(str))
					return e;
			}
		if (file.equals(file_students))
			for (String e : str_students)
				if (e.contains(str))
					return e;

		return null;
	}
	
	public static boolean modify(String str1, String str2) {
		return (str_students.set(str_students.indexOf(str1), str2) != null);
	}
}
