package student;

import java.util.Scanner;

import java.util.concurrent.TimeUnit;

import io.output;

public class Main {

	public static void main(String[] args) {
		int choice = 0;
		String str = null;
		String tempStr = null;
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);

		Date.info_loading();// 数据加载

		System.out.println("欢迎进入学生信息管理系统!\r\n" + "请输入:\r\n" + "1-登录\r\n" + "2-注册");
		choice = sc.nextInt();
		switch (choice) {
		case 1:
			while (true) {
				System.out.println("请输入用户名:");
				str = sc.next();
				System.out.println("请输入密码:");
				tempStr = sc.next();
				if (Date.seek(Date.file_user, str + " " + tempStr) != null) {
					System.out.println("登录成功!请稍候...");
					try {
						TimeUnit.SECONDS.sleep(2);
						;
					} catch (InterruptedException ie) {
					}
					break;
				} else {
					System.out.println("密码错误!");
					System.out.println("请重新输入:");
				}
			}
			break;
		case 2:
			while (true) {
				System.out.println("请输入用户名:");
				str = sc.next();
				System.out.println("请输入密码:");
				tempStr = sc.next();
				System.out.println("请确认您的密码:");
				if (tempStr.equals(sc.next())) {
					output.out_to(Date.file_user, str + " " + tempStr + "\r\n", true);
					System.out.println("注册成功!已为您自动登录...");
					try {
						TimeUnit.SECONDS.sleep(2);
						;
					} catch (InterruptedException ie) {
					}
					break;
				} else {
					System.out.println("密码不一致,请重新输入:");
				}
			}
			break;
		}

		/* 操作 */
		while (true) {
			Date.menu();
			choice = sc.nextInt();
			switch (choice) {
			case 1:// 查看
				Date.display();
				break;
			case 2:// 增添
				System.out.println("请按\"学号,姓名,性别,出生日期,政治面貌,家庭住址,电话,宿舍号\"的格式输入学生信息:");
				str = sc.next();
				if (Date.add(str))
					System.out.println("添加成功!");
				else
					System.out.println("添加失败!");
				break;
			case 3:// 删除
				System.out.println("请输入要删除的学生学号:");
				str = sc.next();
				if ((tempStr = Date.seek(Date.file_students, str)) == null)
					System.out.println("要删除的学生信息不存在!");
				else {
					if (Date.delete(tempStr))
						System.out.println("删除成功!");
					else
						System.out.println("删除失败!");
				}
				break;
			case 4:// 查找
				System.out.println("请输入要查找的学生学号:");
				str = sc.next();
				if ((tempStr = Date.seek(Date.file_students, str)) != null)
					System.out.println(tempStr);
				else
					System.out.println("要查找的学生信息不存在!");
				break;
			case 5:// 修改
				System.out.println("请输入要修改的学生学号:");
				str = sc.next();
				if ((tempStr = Date.seek(Date.file_students, str)) == null)
					System.out.println("要修改的学生信息不存在!");
				else {
					System.out.println("请按\"学号,姓名,性别,出生日期,政治面貌,家庭住址,电话,宿舍号\"的格式输入学生信息:");
					System.out.println("请输入修改后的学生信息");
					if(Date.modify(tempStr, sc.next()))
						System.out.println("修改成功!");
					else
						System.out.println("修改失败!");
				}
				break;
			case 6:// 退出
				Date.flush();
				return;
			default:// 其他输入
				System.out.println("错误输入!\r\n请重新输入:");
			}
		}
	}
}
